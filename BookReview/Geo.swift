//
//  Geo.swift
//  BookReview
//
//  Created by BerSGS on 8/30/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit
import JSONModel

class Geo: JSONModel {
    var lat: String?
    var lng: String?
}
