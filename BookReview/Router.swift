//
//  Router.swift
//  BookReview
//
//  Created by BerSGS on 8/31/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    static let baseURLString = "http://jsonplaceholder.typicode.com"
    
    case Posts
    case ReadPost(String)
    case Users
    case ReadUserPosts(String)
    
    var method: Alamofire.Method {
        switch self {
        case .Posts:
            return .GET
        case .ReadPost:
            return .GET
        case .Users:
            return .GET
        case .ReadUserPosts:
            return .GET

        }
    }
    
    var path: String {
        switch self {
        case .Posts:
            return "/posts"
        case .ReadPost(let post_id):
            return "/posts/\(post_id)"
        case .Users:
            return "/users"
        case .ReadUserPosts(let user_id):
            return "/users/\(user_id)/posts"
        }
    }
    
    // MARK: URLRequestConvertible
    
    var URLRequest: NSMutableURLRequest {
        let URL = NSURL(string: Router.baseURLString)!
        let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path))
        mutableURLRequest.HTTPMethod = method.rawValue
        
        return mutableURLRequest;
        
    }
}