//
//  ReviewTableViewCell.swift
//  BookReview
//
//  Created by BerSGS on 8/31/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel?
    @IBOutlet weak var body: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func displayContent(title: String, body: String){
        self.title?.text = title
        self.body?.text = body
    }

}
