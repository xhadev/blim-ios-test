//
//  User.swift
//  BookReview
//
//  Created by BerSGS on 8/30/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit
import JSONModel

class User: JSONModel {
     var id = 0
     var name: String?
     var username: String?
     var email: String?
     var address = Address()
     var phone: String?
     var website: String?
     var company = Company()
    
}
