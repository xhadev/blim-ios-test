//
//  NetworkingHelper.swift
//  BookReview
//
//  Created by BerSGS on 8/31/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit
import Alamofire

// MARK: - Networking Functions
extension BaseViewController{
    
    
    func fetchAllUsers(completion: ([User]) -> Void){
        
         var users: Array<User> = []

        Alamofire.request(Router.Users).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                let response = JSON as! NSArray
                for item in response { // loop through data items
                    let obj = item as! NSDictionary
                    do {
                        let user = try User(dictionary: obj as [NSObject : AnyObject])
                        //adding the user to array
                        users.append(user)
                    } catch {
                        let message = "Something went wrong mapping the entity"
                        SGLog.logger.severe(message)
                    }
                    
                }

                completion(users)
                
            case .Failure(let error):
                let message = "Request failed with error: \(error)"
                SGLog.logger.severe(message)
              
                 completion(users)
                }
        }
        
    }
    
    
    func fetchPost(postId: String, completionHandler: (Post) -> Void){
        
        var post: Post = Post()
        
        Alamofire.request(Router.ReadPost(postId)).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                let response = JSON as! NSArray
                for item in response { // loop through data items
                    let obj = item as! NSDictionary
                    do {
                        post = try Post(dictionary: obj as [NSObject : AnyObject])
                        
                    } catch {
                        let message = "Something went wrong mapping the entity"
                        SGLog.logger.severe(message)
                    }
                    
                    
                }
                
                completionHandler(post)
                
            case .Failure(let error):
                let message = "Request failed with error: \(error)"
                SGLog.logger.severe(message)
                completionHandler(post)
                }
        }
        
    }
    
    func fetchAllPosts(completionHandler: ([Post]) -> Void){
        
        var posts: Array<Post> = []
        
        Alamofire.request(Router.Posts).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                let response = JSON as! NSArray
                for item in response { // loop through data items
                    let obj = item as! NSDictionary
                    do {
                        let post = try Post(dictionary: obj as [NSObject : AnyObject])
                        //adding the user to array
                        posts.append(post)
                    } catch {
                        let message = "Something went wrong mapping the entity"
                        SGLog.logger.severe(message)
                    }
                    
                    
                }
                
                completionHandler(posts)
                
            case .Failure(let error):
                let message = "Request failed with error: \(error)"
                SGLog.logger.severe(message)
                 completionHandler(posts)
                }
        }
        
    }
    
    func fetchUserPosts(userId: String, completionHandler: ([Post]) -> Void){
        
        var posts: Array<Post> = []
        
        Alamofire.request(Router.ReadUserPosts(userId)).responseJSON
            { response in switch response.result {
            case .Success(let JSON):
                let response = JSON as! NSArray
                for item in response { // loop through data items
                    let obj = item as! NSDictionary
                    do {
                        let post = try Post(dictionary: obj as [NSObject : AnyObject])
                        //adding the user to array
                        posts.append(post)
                    } catch {
                        let message = "Something went wrong mapping the entity"
                        SGLog.logger.severe(message)
                    }
                    
                    
                }
                
                completionHandler(posts)
                
            case .Failure(let error):
                let message = "Request failed with error: \(error)"
                SGLog.logger.severe(message)
                completionHandler(posts)
                }
        }
        
    }
    
    
}