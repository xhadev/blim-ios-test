//
//  Address.swift
//  BookReview
//
//  Created by BerSGS on 8/30/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit
import JSONModel

/*"address": {
    "street": "Kulas Light",
    "suite": "Apt. 556",
    "city": "Gwenborough",
    "zipcode": "92998-3874",
 
},*/

class Address: JSONModel {

    var geo = Geo()
    
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
}
