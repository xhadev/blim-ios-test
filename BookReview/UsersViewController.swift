//
//  SecondViewController.swift
//  BookReview
//
//  Created by Angel Casado on 2/1/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit
import Alamofire

class UsersViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var tableView: UITableView!
    var selectedRow: Int?
    var users: Array<User> = []
     var needToReload: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
       
    }
    
    override func viewDidAppear(animated: Bool) {
        //Need for deselect the rows when the main view controller is not UITableViewController
        if self.tableView.indexPathForSelectedRow?.row != nil {
            self.tableView.deselectRowAtIndexPath(self.tableView.indexPathForSelectedRow!, animated: true)
        }
        
        if needToReload {
            //Retrieve all the user
            loadUsers()
        }
        
    }
    
    func  loadUsers() -> Void {
        //1 Show the loader in order to tell the user that a long task is executing
        self.showLoader()
        
        //2 Clean the Array to get fresh values
        self.users.removeAll()
        
        //3 Create and call the API Using Alamofire and retrieving the response in callback
        fetchAllUsers { (data) in
            //This response is received in the main Thread.
            //4. Stop the loader
            self.stopLoader()
            //5. Assigning the response to the Array that hold the entities.
            self.users = data
            //6. Call the reloadData method in order to fire the cellForRowAtIndexPath and show the retrieved Data.
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserTableViewCell", forIndexPath: indexPath) as UITableViewCell
        
        cell.textLabel?.text = self.users[indexPath.row].name
        return cell
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        selectedRow = self.tableView.indexPathForSelectedRow?.row
        if (segue.identifier == "segueUserReviews") {
            needToReload = false
            let reviewsViewController = segue.destinationViewController as! ReviewsViewController;
            //gettin the user selected.
            let user = self.users[selectedRow!]
            //Setting the user for retrieve the reviews.
            reviewsViewController.selectedUser = user
            //Setting the flago in order to render the user Reviews
            reviewsViewController.reviewType = ReviewTypes.UserReviews
        }
    }
    

}

