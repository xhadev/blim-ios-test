//
//  ReviewsViewController.swift
//  BookReview
//
//  Created by BerSGS on 8/30/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit
import Alamofire

enum ReviewTypes {
    case AllReviews
    case UserReviews
}

class ReviewsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var posts: Array<Post> = []
    var selectedRow: Int?
    var reviewType: ReviewTypes = ReviewTypes.AllReviews
    var selectedUser: User?
    var needToReload: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.allowsSelection = true

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.estimatedRowHeight = 200;
        
        setupViewController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {

        if self.tableView.indexPathForSelectedRow?.row != nil{
            self.tableView.deselectRowAtIndexPath(self.tableView.indexPathForSelectedRow!, animated: true)
        }

        if needToReload {
            loadPostElements()
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //1 Deque the ReviewTableViewCell the optimize way to get cell At Index Path
        let cell = tableView.dequeueReusableCellWithIdentifier("ReviewTableViewCell", forIndexPath: indexPath) as! ReviewTableViewCell
        
        //2 Fetches the appropriate post for the data source layout.
        let post = self.posts[indexPath.row]
        
        //3 Fill the data into the cell.
        cell.displayContent( post.title!, body: post.body! )
        
        //4 return the cell.
        return cell
    }
    
    func loadPostElements(){
        showLoader()
        switch self.reviewType {
        case .AllReviews:
            //3 Create and call the API Using Alamofire and retrieving the response in callback
            fetchAllPosts { (data) in
                //This response is received in the main Thread.
                //4. Stop the loader
                self.stopLoader()
                //5. Assigning the response to the Array that hold the entities.
                self.posts = data
                //6. Call the reloadData method in order to fire the cellForRowAtIndexPath and show the retrieved Data.
                self.tableView.reloadData()
            }
        case .UserReviews:
            let userId = String(self.selectedUser!.id)
            
            //3 Create and call the API Using Alamofire and retrieving the response in callback
            fetchUserPosts(userId, completionHandler: { (data) in
                //This response is received in the main Thread.
                //4. Stop the loader
                self.stopLoader()
                //5. Assigning the response to the Array that hold the entities.
                self.posts = data
                //6. Call the reloadData method in order to fire the cellForRowAtIndexPath and show the retrieved Data.
                self.tableView.reloadData()
            })
        }
    }
    
    func setupViewController(){
        //Setting the title according to the Type of the reviews.
        switch self.reviewType {
        case .AllReviews:
            self.navigationItem.title = String("Reviews")
        case .UserReviews:
           self.navigationItem.title = String("Reviews by "+(selectedUser?.name)!)
        }
        
                    

    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        selectedRow = self.tableView.indexPathForSelectedRow?.row
        if (segue.identifier == "segueReviewDetail") {
            needToReload = false
            let reviewDetailViewController = segue.destinationViewController as! ReviewDetailViewController;
            let post = self.posts[selectedRow!]
            reviewDetailViewController.post = post
        }
    }
    
    
}
