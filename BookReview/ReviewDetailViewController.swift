//
//  ReviewDetailViewController.swift
//  BookReview
//
//  Created by BerSGS on 8/31/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit

class ReviewDetailViewController: UIViewController {
    
    @IBOutlet var reviewDetail: UITextView!

    var currentAttributedText : NSMutableAttributedString?
    var post: Post?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //displayReview()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        displayReview()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        currentAttributedText = attributedText()
    }
    
    func displayReview(){
        
       reviewDetail.allowsEditingTextAttributes = true
        
       reviewDetail.attributedText = currentAttributedText


    }
    
    func attributedText()-> NSMutableAttributedString {
        
        
        let reviewTitle = post!.title
        let reviewBody = post!.body
        
        
        let attrs = [NSFontAttributeName : UIFont.boldSystemFontOfSize(14)]
        let attributedString = NSMutableAttributedString(string:reviewTitle!, attributes:attrs)
        
        attributedString.appendAttributedString(NSMutableAttributedString(string:"\n\n"))
        
         let bodyAttrs = [NSFontAttributeName : UIFont.systemFontOfSize(12)]
        
        attributedString.appendAttributedString(NSMutableAttributedString(string:reviewBody!, attributes: bodyAttrs))
        
        return attributedString
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
