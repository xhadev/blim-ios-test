//
//  SGLog.swift
//  BookReview
//
//  Created by BerSGS on 8/30/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import Foundation
import XCGLogger

class SGLog {
    static let logger = XCGLogger(identifier: "advancedLogger", includeDefaultDestinations: false)
    
    static var logFilePath : String {
        get {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
             return "\(documentsPath)/BookReview.log"
        }
    }
    
    static func enableFileLog(){
        
        // Create a file log destination
        let fileLogDestination = XCGFileLogDestination(owner: logger, writeToFile:logFilePath , identifier: "advancedLogger.fileLogDestination")
        print(logFilePath)
        // Optionally set some configuration options
        fileLogDestination.outputLogLevel = .Debug
        fileLogDestination.showLogIdentifier = false
        fileLogDestination.showFunctionName = true
        fileLogDestination.showThreadName = true
        fileLogDestination.showLogLevel = true
        fileLogDestination.showFileName = true
        fileLogDestination.showLineNumber = true
        fileLogDestination.showDate = true
        
        // Process this destination in the background
        fileLogDestination.logQueue = XCGLogger.logQueue
        
        // Add the destination to the logger
        logger.addLogDestination(fileLogDestination)
        
        // Add basic app info, version info etc, to the start of the logs
        logger.logAppDetails()
    }
}