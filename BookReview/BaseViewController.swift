//
//  BaseViewController.swift
//  BookReview
//
//   Base class created to host common functionalities used accross the ViewControllers.
//
//  Created by BerSGS on 8/30/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController, NVActivityIndicatorViewable {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    func showLoader(){
        let size = CGSize(width: 50, height:50)
        startActivityAnimating(size, message: "", type: NVActivityIndicatorType(rawValue: 29)!)
    }
    
    func stopLoader() {
        stopActivityAnimating()
    }
    
    
    func showAlert(withTitle title: String, message:String, isError :Bool){
        let alert = UIAlertController(title:(isError) ? "Error!" : title, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            UIApplication.sharedApplication().delegate?.window!!.rootViewController?.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
