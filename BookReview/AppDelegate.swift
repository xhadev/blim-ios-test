//
//  AppDelegate.swift
//  BookReview
//
//  Created by Angel Casado on 2/1/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit
import XCGLogger

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        
        //Setup the Logger Config
        setUpLoggerConfig()
        
        //Setup the logger to log Unhandled crash
        logUnhandledCrashes()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func logUnhandledCrashes(){
        NSSetUncaughtExceptionHandler { exception in
            SGLog.logger.severe("NAME : \(exception.name)\nREASON : \(exception.reason)\nUserInfo: \(exception.userInfo)\nStackSymbols:\(exception.callStackSymbols)")
        }
        
        //NOT HANDLED.
        signal(SIGABRT, { signal in })
        signal(SIGILL, { signal in  })
        signal(SIGSEGV, { signal in  })
        signal(SIGFPE, { signal in  })
        signal(SIGBUS, { signal in  })
        signal(SIGPIPE, { signal in  })
    }
    
    
    func signalLog(reason : String, signal : Int32) {
        SGLog.logger.severe("NAME : SIGNAL CRASH ***\(signal)***\nREASON : \(reason))")
    }
    
    
    
    func setUpLoggerConfig(){
        SGLog.logger.setup(.Verbose, showLogIdentifier: false, showFunctionName: true, showThreadName: false, showLogLevel: true, showFileNames: true, showLineNumbers: false, showDate: true, writeToFile: nil, fileLogLevel: .Verbose)
        SGLog.logger.xcodeColorsEnabled = true
        SGLog.logger.xcodeColors = [ .Verbose: .lightGrey,
                                     .Debug: .darkGreen,
                                     .Info: .darkGrey,
                                     .Warning: .orange,
                                     .Error: .black, // Optionally use a UIColor
            .Severe: .red // Optionally use RGB values directly
        ]
        SGLog.enableFileLog()
    }


}

