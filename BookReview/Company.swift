//
//  Company.swift
//  BookReview
//
//  Created by BerSGS on 8/30/16.
//  Copyright © 2016 Televisa. All rights reserved.
//

import UIKit
import JSONModel

class Company: JSONModel {
    var name: String?
    var catchPhrase: String?
    var bs: String?
}
